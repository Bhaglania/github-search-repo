package anupam.com.githubsearchrepo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import anupam.com.githubsearchrepo.modals.Contributor;
import anupam.com.githubsearchrepo.modals.Repository;
import anupam.com.githubsearchrepo.myAdapters.ContributorGridViewAdaptor;
import anupam.com.githubsearchrepo.myUtils.DownloadDataTask;

public class GitRepoDetailsActivity extends AppCompatActivity
        implements DownloadDataTask.OnDataDownloadListener {
    ProgressDialog progress;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_git_repo_details);


        final Repository repository = (Repository) getIntent().getExtras().get("repoDetails");

        addRepositoryToView(repository);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(repository.getName());
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    private void addRepositoryToView(final Repository repository) {
        Context context = getApplicationContext();
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        TextView textView = (TextView) findViewById(R.id.repoName);
        TextView description = (TextView) findViewById(R.id.description);
        LinearLayout projectLink = (LinearLayout) findViewById(R.id.projectLink);
        projectLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = repository.getProjectLink();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        Picasso.with(context).load(repository.getImageUrl()).placeholder
                (ContextCompat.getDrawable(context, R.drawable.ic_blur_on_black)).into(imageView);

        textView.setText(repository.getName());
        description.setText("Description:\n" + repository.getDescription());

        downloadData(repository.getContributorUrl());
    }

    private void downloadData(String url) {
        progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setCancelable(false);
        progress.setMessage("Wait while loading...");
        progress.show();
        new DownloadDataTask(this, url).execute();
    }

    @Override
    public void onDataDownloadSuccess(JSONObject result) {

    }

    @Override
    public void onDataDownloadSuccess(String result) {
        GridView gridView = (GridView) findViewById(R.id.contributors);

        try {
            ArrayList<Contributor> contributors = getContributors(result);
            gridView.setAdapter(new ContributorGridViewAdaptor(getApplicationContext(), contributors));
            scrollToTop();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        progress.dismiss();
    }

    private void scrollToTop() {
        final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }
        });
    }


    private ArrayList<Contributor> getContributors(String result) throws JSONException {
        JSONArray jsonArray = new JSONArray(result);
        ArrayList<Contributor> contributors = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
            String name = jsonObject.optString("login");
            String imageUrl = jsonObject.getString("avatar_url");
            String reposUrl = jsonObject.getString("repos_url");
            Contributor contributor = new Contributor();
            contributor.setName(name);
            contributor.setImageUrl(imageUrl);
            contributor.setReposUrl(reposUrl);
            contributors.add(contributor);
        }
        return contributors;
    }

    @Override
    public void onDataDownloadError(String errorDescription) {

    }
}
