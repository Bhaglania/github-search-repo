package anupam.com.githubsearchrepo;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import anupam.com.githubsearchrepo.modals.Contributor;
import anupam.com.githubsearchrepo.myAdapters.ContributorReposRecyclerViewAdapter;
import anupam.com.githubsearchrepo.myUtils.DownloadDataTask;

public class GitContributorDetailsActivity extends AppCompatActivity
        implements DownloadDataTask.OnDataDownloadListener {

    ProgressDialog progress;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_git_contibutor_details);
        Context context = this;
        Contributor contributor = (Contributor) getIntent().getExtras().get("contributor");

        ImageView imageView = (ImageView) findViewById(R.id.imageView);

        Picasso.with(context).load(contributor.getImageUrl()).placeholder
                (ContextCompat.getDrawable(context, R.drawable.ic_blur_on_black)).into(imageView);

        downloadData(contributor.getReposUrl());

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(contributor.getName());
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void downloadData(String url) {
        progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.show();
        new DownloadDataTask(this, url).execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onDataDownloadSuccess(JSONObject result) {

    }

    @Override
    public void onDataDownloadSuccess(String result) {
        progress.dismiss();
        try {
            JSONArray jsonArray = new JSONArray(result);
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            ContributorReposRecyclerViewAdapter repositoriesAdaptor = new ContributorReposRecyclerViewAdapter(getApplicationContext(), jsonArray);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(repositoriesAdaptor);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDataDownloadError(String errorDescription) {

    }
}
