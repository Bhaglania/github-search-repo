package anupam.com.githubsearchrepo.myAdapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import anupam.com.githubsearchrepo.GitRepoDetailsActivity;
import anupam.com.githubsearchrepo.R;
import anupam.com.githubsearchrepo.modals.Repository;


public class RepositoriesAdaptor extends RecyclerView.Adapter<RepositoriesAdaptor.MyViewHolder> {

    Context context;
    ArrayList<Repository> repositories;

    public RepositoriesAdaptor(Context context, ArrayList<Repository> repositories) {
        this.context = context;
        this.repositories = repositories;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repo_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Repository repository = repositories.get(position);
        holder.name.setText(repository.getName());
        holder.fullName.setText(repository.getFullName());
        Drawable drawable = ContextCompat.getDrawable(context, R.drawable.ic_watchers);
        holder.watcherCount.setText("" + repository.getWatcherCount());
        holder.watcherCount.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, null, null, null);

        drawable = ContextCompat.getDrawable(context, R.drawable.ic_fork);
        holder.forksCount.setText("" + repository.getForksCount());
        holder.forksCount.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, null, null, null);

        Picasso.with(context).load(repository.getImageUrl()).placeholder
                (ContextCompat.getDrawable(context, R.drawable.ic_blur_on_black)).into(holder.imageView);
        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, GitRepoDetailsActivity.class);
                intent.putExtra("repoDetails", repository);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, fullName, watcherCount, forksCount;
        ImageView imageView;
        LinearLayout parent;

        MyViewHolder(View itemView) {
            super(itemView);
            parent = (LinearLayout) itemView.findViewById(R.id.parent);
            name = (TextView) itemView.findViewById(R.id.name);
            fullName = (TextView) itemView.findViewById(R.id.fullName);
            watcherCount = (TextView) itemView.findViewById(R.id.watcherCount);
            forksCount = (TextView) itemView.findViewById(R.id.forksCount);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
        }
    }
}
