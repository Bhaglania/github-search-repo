package anupam.com.githubsearchrepo;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import anupam.com.githubsearchrepo.modals.Repository;
import anupam.com.githubsearchrepo.myAdapters.RepositoriesAdaptor;
import anupam.com.githubsearchrepo.myUtils.DownloadDataTask;

public class SearchRepositoryActivity extends AppCompatActivity
        implements DownloadDataTask.OnDataDownloadListener {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_repository);
        searchRepository("android image");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            searchRepository(query);
        }
    }

    ProgressDialog progress;

    void searchRepository(String query) {
        progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.show();
        query = query.replace(" ", "%20");
        String url = "https://api.github.com/search/repositories?q=" + query +
                "&sort=watchers_count&order=desc";
        new DownloadDataTask(this, url).execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.search:
                onSearchRequested();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onDataDownloadSuccess(JSONObject result) {
        try {
            JSONArray jsonArray = result.getJSONArray("items");
            ArrayList<Repository> repositories = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                Repository repository = getRepositoryFromJSON(jsonObject);
                repositories.add(repository);
                if (i == 10) {
                    i = jsonArray.length() + 2;
                }
            }
            addRepositoriesToView(repositories);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        progress.dismiss();
    }

    @Override
    public void onDataDownloadSuccess(String result) {

    }

    private void addRepositoriesToView(ArrayList<Repository> repositories) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        RepositoriesAdaptor repositoriesAdaptor = new RepositoriesAdaptor(getApplicationContext(), repositories);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(repositoriesAdaptor);

    }

    public static Repository getRepositoryFromJSON(JSONObject jsonObject) {
        Repository repository = new Repository();
        String name = jsonObject.optString("name");
        String fullName = jsonObject.optString("full_name");
        int watchCount = jsonObject.optInt("watchers_count", 0);
        int forks_count = jsonObject.optInt("forks_count", 0);
        String contributorsUrl = jsonObject.optString("contributors_url");
        String description = jsonObject.optString("description");
        String projectLink = jsonObject.optString("html_url");
        String imageUrl = "";
        try {
            imageUrl = jsonObject.optJSONObject("owner").getString("avatar_url");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        repository.setImageUrl(imageUrl);
        repository.setDescription(description);
        repository.setProjectLink(projectLink);
        repository.setForksCount(forks_count);
        repository.setWatcherCount(watchCount);
        repository.setContributorUrl(contributorsUrl);
        repository.setName(name);
        repository.setFullName(fullName);
        return repository;
    }

    @Override
    public void onDataDownloadError(String errorDescription) {

    }
}
