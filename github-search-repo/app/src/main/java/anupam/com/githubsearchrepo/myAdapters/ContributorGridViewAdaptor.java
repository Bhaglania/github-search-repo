package anupam.com.githubsearchrepo.myAdapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import anupam.com.githubsearchrepo.myUtils.CircleTransform;
import anupam.com.githubsearchrepo.GitContributorDetailsActivity;
import anupam.com.githubsearchrepo.R;
import anupam.com.githubsearchrepo.modals.Contributor;


public class ContributorGridViewAdaptor extends BaseAdapter {
    Context context;
    ArrayList<Contributor> contributors;

    public ContributorGridViewAdaptor(Context context, ArrayList<Contributor> contributors) {
        this.context = context;
        this.contributors = contributors;
    }

    @Override
    public int getCount() {
        return contributors.size();
    }

    @Override
    public Object getItem(int i) {
        return contributors.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View itemView, ViewGroup viewGroup) {
        final Contributor co = contributors.get(i);
        if (itemView == null) {
            itemView = LayoutInflater.from(context)
                    .inflate(R.layout.contributor_item, null, false);
        }

        TextView textView = (TextView) itemView.findViewById(R.id.textView);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        textView.setText("" + co.getName());
        Picasso.with(context).load(co.getImageUrl()).
                transform(new CircleTransform())
                .resize(100, 100)
                .placeholder(ContextCompat.getDrawable(context, R.drawable.ic_blur_on_black_small))
                .into(imageView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GitContributorDetailsActivity.class);
                intent.putExtra("contributor", co);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        return itemView;
    }
}
