package anupam.com.githubsearchrepo.myAdapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import anupam.com.githubsearchrepo.GitRepoDetailsActivity;
import anupam.com.githubsearchrepo.R;
import anupam.com.githubsearchrepo.SearchRepositoryActivity;
import anupam.com.githubsearchrepo.modals.Repository;


public class ContributorReposRecyclerViewAdapter extends RecyclerView.Adapter<ContributorReposRecyclerViewAdapter.MyViewHolder> {

    Context context;
    JSONArray repositories = new JSONArray();

    public ContributorReposRecyclerViewAdapter(Context context, JSONArray repositories) {
        this.context = context;
        this.repositories = repositories;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        try {
            final JSONObject jsonObject = (JSONObject) repositories.get(position);
            holder.name.setText(jsonObject.optString("name"));
            holder.name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Repository repository = SearchRepositoryActivity.getRepositoryFromJSON(jsonObject);
                    Intent intent = new Intent(context, GitRepoDetailsActivity.class);
                    intent.putExtra("repoDetails", repository);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return repositories.length();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;

        MyViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);

        }
    }
}
